import React, { Component } from 'react';
import './App.css';
import Plot from './Plot.js';
import {connect} from 'react-redux';
import {
    changeLocation,
    setSelectedDate,
    setSelectedTemp,
    fetchData
} from "./actions";

class App extends Component {


  fetchData = (evt) => {
    evt.preventDefault();
    console.log("fetchData for", this.props.location);

    var urlPrefix = "http://api.openweathermap.org/data/2.5/forecast?q=";
    var urlSuffix = "&APPID=644e04b6b88c86dce927eadf172bf51d&units=metric";
    var location = encodeURIComponent(this.props.location);
    var url = urlPrefix + location + urlSuffix;

    this.props.dispatch(fetchData(url));
  };

  changeLocation = (evt) => {
      this.props.dispatch(changeLocation(evt.target.value))
  };


  onPlotClick = (data) => {
    console.log("on plot click", data);
    if(data.points) {
        // var number = data.points[0].pointNumber;
        this.props.dispatch(setSelectedDate(data.points[0].x));
        this.props.dispatch(setSelectedTemp(data.points[0].y));
    }
  };

  render() {
    var currentTemp = 'not Loaded';
    if (this.props.data.list) {
      currentTemp = this.props.data.list[0].main.temp;
    }
    return (
      <div className="App">
       <h1>Weather App!</h1>
        <form onSubmit={this.fetchData}>
          <label>I want to know the weather for
          <input
              type="text"
              placeholder={"City, Country"}
              value={this.props.location}
              onChange={this.changeLocation}
          />
          </label>
        </form>
        {(this.props.data.list) ?
          <div className={"wrapper"}>
            <p className={"temp-wrapper"}>
                <span className={"temp"}>{
                  (this.props.selected.temp) ? this.props.selected.temp : currentTemp}
                </span>
                <span className={"temp-symbol"}>°C </span>
                <span className={"temp-date"}>
                  {(this.props.selected.temp) ? this.props.selected.date : ''}
                </span>
            </p>
            <h2>Forecast</h2>
            <Plot
              xData={this.props.dates}
              yData={this.props.temps}
              type={"scatter"}
              onPlotClick={this.onPlotClick}
            />
          </div>
            : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
    return state.toJS();
}

export default connect(mapStateToProps)(App);
