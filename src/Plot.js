/* global Plotly */
import React from 'react';

class Plot extends React.Component {
    render() {
        console.log('RENDER PLOT');
        return (
            <div id={"plot"}></div>
        );
    }

    drawPlot = () => {
        Plotly.newPlot('plot', [{
            x: this.props.xData,
            y: this.props.yData,
            type: this.props.type
        }], {
            margin: {
                t: 0, r: 0, l: 30
            },
            xaxis: {
                gridcolor: 'transparent'
            }
        }, {
            displayModeBar: false
        });
        document.getElementById('plot').on('plotly_click', this.props.onPlotClick);
    }

    componentDidMount() {
        this.drawPlot();
    }

    componentDidUpdate() {
        this.drawPlot();
    }
}

export default Plot;